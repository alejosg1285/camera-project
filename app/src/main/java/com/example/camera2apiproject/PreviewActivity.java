package com.example.camera2apiproject;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

public class PreviewActivity extends AppCompatActivity {
    private ImageView imgPreview;
    private ImageButton imgReturn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);

        String pathImage =getIntent().getStringExtra("pathImage");

        Bitmap bitmap = BitmapFactory.decodeFile(pathImage);

        imgPreview = findViewById(R.id.imgPreview);
        imgPreview.setImageBitmap(bitmap);

        imgReturn = findViewById(R.id.imgReturn);
        imgReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PreviewActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
    }
}
